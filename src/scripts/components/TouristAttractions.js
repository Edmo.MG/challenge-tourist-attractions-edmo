export class TouristAttractions {
    constructor() {
        this.selectores();
        this.events();
    }

    selectores() {
        this.photo = document.getElementById("preview-image");
        this.file = document.getElementById("image-input");
        this.label = document.querySelector(".tourist-form-image-label");
    }

    events() {
        this.photo.addEventListener("click", () => {
            this.file.click();
        });

        this.previewImage();
    }

    previewImage() {
        this.file.addEventListener("change", (event) => {
            this.reader = new FileReader();

            this.reader.onload = () => {
                this.photo.src = this.reader.result;
            };
            this.label.style.display = "none";

            this.reader.readAsDataURL(this.file.files[0]);
        });
    }
}
