export class TouristList {
    constructor() {
        this.list = [];

        this.selectors();
        this.events();
    }

    selectors() {
        this.form = document.querySelector(".tourist-form");
        this.imageInput = document.querySelector(".tourist-form-image-input");
        this.tittleInput = document.querySelector(".tourist-form-tittle-input");
        this.descriptionInput = document.querySelector(".tourist-form-description-input");
        this.previewImage = document.querySelector(".tourist-image-preview");
        this.items = document.querySelector(".tourist-result-list");
    }

    events() {
        this.form.addEventListener("submit", this.addItemToList.bind(this));
    }

    addItemToList(event) {
        event.preventDefault();

        const itemImage = event.target["preview-image"].src;
        const itemTittle = event.target["tittle-name"].value;
        const itemDescription = event.target["description-name"].value;

        const item = {
            image: itemImage,
            tittle: itemTittle,
            description: itemDescription,
        };

        this.list.push(item);
        this.renderListItems();
        this.resetInputs();
    }

    renderListItems() {
        let itemsStructure = "";

        this.list.forEach(function (item) {
            itemsStructure += `
                  <li class="tourist-item-list">
                    <div class="tourist-item-card">
                      <div class="tourist-image-card">
                        <img class="tourist-image-result" src="${item.image}"/>
                      </div>
                      <div class="tourist-card-text">
                        <h1>${item.tittle}</h1>
                        <p>${item.description}</p>
                    </div>
                    </div>
                  </li>
              `;
        });

        this.items.innerHTML = itemsStructure;
    }

    resetInputs() {
        this.imageInput.src = "";
        this.tittleInput.value = "";
        this.descriptionInput.value = "";
    }
}
