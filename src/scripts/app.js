import { TouristAttractions } from "./components/TouristAttractions";

import { TouristList } from "./components/TouristList";

document.addEventListener("DOMContentLoaded", function () {
    new TouristAttractions();
    new TouristList();
});
