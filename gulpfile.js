const { src, dest, watch, parallel } = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const browserify = require("browserify");
const babelify = require("babelify");
const source = require("vinyl-source-stream");
const buffer = require("vinyl-buffer");
const uglify = require("gulp-uglify");
const imagemin = require("gulp-imagemin");
const connect = require("gulp-connect");

function server() {
    connect.server({
        root: "dist",
        livereload: true,
        port: 3000,
    });
}

function sentinel() {
    watch("src/templates/**/*.html", { ignoreInitial: false }, html);
    watch("src/styles/**/*.scss", { ignoreInitial: false }, styles);
    watch("src/scripts/**/*.js", { ignoreInitial: false }, scripts);
    watch("src/styles/assets/images/**/*", { ignoreInitial: false }, images);
}

function html() {
    return src("src/templates/**/*.html").pipe(dest("dist"));
}

function images() {
    return src("src/styles/assets/images/**/*").pipe(imagemin()).pipe(dest("dist/images"));
}

function styles() {
    return src("src/styles/main.scss")
        .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
        .pipe(dest("dist"))
        .pipe(connect.reload());
}

function scripts() {
    return browserify("src/scripts/app.js")
        .transform("babelify", { presets: ["@babel/preset-env"] })
        .bundle()
        .pipe(source("bundle.js"))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(dest("dist"))
        .pipe(connect.reload());
}

exports.default = parallel(server, sentinel);
